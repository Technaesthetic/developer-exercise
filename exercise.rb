class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    output = str.gsub(/\w{5,}/) do |match|
      rep = 'marklar'
      if match.capitalize! == nil
        rep.capitalize
      else
        rep
      end
    end
    output
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.fibonacci(n)
    if n < 2
      n
    else
      self.fibonacci(n-1) + self.fibonacci(n-2)
    end
  end

  def self.even_fibonacci(nth)
    output = 0
    (0..nth).each do |n|
      num = self.fibonacci(n)
      if num.even? == true
        output += num
      end
    end
    output
  end

end
